% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/tsFunctions.R
\name{getProportion}
\alias{getProportion}
\title{Compute the proportion of the population that is meeting physical activity guidelines through active transport.}
\usage{
getProportion(ts, minimum = 7.5, byLocation = TRUE)
}
\arguments{
\item{ts}{A TravelSurvey object}

\item{minimum}{The minimum activity threshold of MET-hrs/week that persons should meet in order to avoid health risks. Default is 7.5 based on LTPA recommendation.}

\item{byLocation}{Boolean dictating whether proportion returned will be singular value for overall TS object, or values for each location}
}
\value{
Percentage of sample meeting minimum activity. If stratified by location, a dataframe with columns location and proportion. If not, a singular value.
}
\description{
Compute the proportion of the population that is meeting physical activity guidelines through active transport.
}
