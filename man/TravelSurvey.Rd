% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/AllClasses.R
\docType{class}
\name{TravelSurvey}
\alias{TravelSurvey}
\alias{TravelSurvey-class}
\title{The TravelSurvey class}
\description{
This class defines the format for the travel survey data object.
}
\details{
The TravelSurvey object is designed to effectively store all relevant data from a given survey of a population\'s travel activity. This formulated TravelSurvey object is intended for analysis using the HOT R package. See online tutorial for more information: https://ghi-uw.gitlab.io/hot-tutorial/.
}
