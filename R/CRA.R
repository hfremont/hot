#' Perform Comparative Risk Assessment
#'
#' This function accepts a TravelSurvey class object for performing CRA across locations within that object. A user may provide a 'scenario' dataframe for calculating such health outcomes. If no scenario is provided, the CRA wrapper function is run returning a data frame of results for Scenarios 1 (participation = 1) and 2 (participation = 0). If a scenario is provided, the user may specify boolean argument 'compare' to dictate whether their scenario results should be compared against Scenarios 1 and 2.
#'
#' @param ts TravelSurvey object
#' @param R Relative risk function in terms of total exposure (physical activity)
#' @param participation_scenario A numeric variable that defines the participation in the scenario.  Note that this value is used for every location.
#' 
#' @return Data frame of locations and associated PAF results
#' @export
getCRA <- function(ts, R = ERF.acm.arem.fun, participation_scenario = 1){
  
  part.df <- getParticipation(ts)
  intensity.df <- getIntensity(ts)
  
  results <- CRA.full(location = part.df$location,
                      participation.b = part.df$participation,
                      intensity.b = intensity.df,
                      participation.s = rep(participation_scenario, nrow(part.df)),
                      intensity.s = intensity.df,
                      R = R)
  
  return(results)
}
#' Perform Comparative Risk Assessment
#'
#' This function accepts up 5 parameters, all comprised of vectors of values through which to be iterated in order to compute rho (risk, or health burden aversion) using CRA.function.

#' @param location Vector of location values
#' @param participation.b Vector of corresponding baseline participation (pi1) values
#' @param intensity.b Data frame of corresponding baseline intensity values: mean (mu1), sd (sigma1)
#' @param participation.s Vector of corresponding scenario participation (pi1) values
#' @param intensity.s Data frame of corresponding scenario intensity values: mean (mu1), sd (sigma1)
#' @param R Relative risk function in terms of total exposure (physical activity)
#' @param author Literature referenced for physical activity distribution
#' @param income If author Lear, income must be specified for determining physical activity distribution
#' 
#' @return Data frame of locations and associated PAF results
#' @export
CRA.full <- function(location,
                     participation.b,
                     intensity.b,
                     participation.s = rep(0,length(location)), # Default of Scenario 1: participation is 0.
                     intensity.s = intensity.b, # Default of Scenarios 1 and 2: conditional mean/sd remain the same as baseline.
                     R = ERF.acm.arem.fun, # Arem exposure-response function
                     author = "Arem",
                     income = NULL){
  
  # Initialize accumulator vectors
  loc.append <- factor()
  rho.append <-c()
  
  # Iterate through corresponding rows of location, participation, intensity (mean, sd)
  for(i in 1:length(location)) {
    
    # Initialize values for efficient calculation
    loc <- location[i]
    part.b <- participation.b[i]
    part.s <- participation.s[i]
    mean.b <- intensity.b$mean[i]
    sd.b <- intensity.b$sd[i]
    mean.s <- intensity.s$mean[i]
    sd.s <- intensity.s$sd[i]
    
    # Account for NA value(s) of intensity (e.g. location has no active travelers)
    if( NA %in% c(mean.b, sd.b, mean.s, sd.s) ){rho.value <- NA}
    else{
      
      # Calculate Physical Activity (P) mean and sd given author and income, if applicable (Lear)
      PA <- getPAIntensity(author, income)
      mean.P <- PA$mean
      sd.P <- PA$sd
      
      # Account for Arem and Wen -- authors' values are for leisure activity, not physical activity
      if( author == "Lear" ){
        mean.Tc <- mean.P - part.b*mean.b # use mean/sd of baseline T
        sd.Tc <- mean.Tc*(sd.P/mean.P) } # assume cv from P to calculate standard deviation of Non-travel Activity (Tc)
      else{ mean.Tc <- mean.P ; sd.Tc <- sd.P }
      
      # Perform CRA for each set of parameters
      rho.value <- CRA.function(participation.baseline = part.b,
                                participation.scenario = part.s,
                                meanlog.baseline = log(mean.b^2 / sqrt(sd.b^2 + mean.b^2)),
                                sdlog.baseline = sqrt(log(1 + (sd.b / mean.b)^2)),
                                meanlog.scenario = log(mean.s^2 / sqrt(sd.s^2 + mean.s^2)),
                                sdlog.scenario = sqrt(log(1 + (sd.s / mean.s)^2)),
                                mean.Tc = mean.Tc,
                                sd.Tc = sd.Tc,
                                Tc.method = "Multinomial", # future avenue: iterate through values of Tc.method
                                author = author,
                                income = income,
                                R = R)
    }
    
    # Append values to accumulation vectors
    loc.append <- unlist(list(loc.append, loc))
    rho.append <- c(rho.append, -rho.value)
  }
  
  # Combine accumulation vectors into final results data frame
  results <- data.frame(location = loc.append, PAF = rho.append)
  return(results)
}
#' Perform Comparative Risk Assessment
#'
#' This function accepts up to eight parameters for use with the
#' parameteric physical activity model, or two vectors of quantiles
#' for the non-parametric model.  The function returns the population
#' attributable fraction.
#'
#' @param meanlog.baseline Mean of exposure in baseline (log-scale, parameteric model)
#' @param sdlog.baseline Standard deviation of exposure in baseline (log-scale, parameteric model)
#' @param participation.baseline Participation, or proportion of active travelers in baseline (pi1)
#' @param meanlog.scenario Mean of exposure in scenario (log-scale, parameteric model)
#' @param sdlog.scenario Standard deviation of exposure in scenario (log-scale, parameteric model)
#' @param participation.scenario Participation, or proportion of active travelers in scenario (pi1)
#' @param Tc.method Distribution of non-travel activity
#' @param author Literature referenced for physical activity distribution
#' @param income If author Lear, income must be specified for determining physical activity distribution
#' @param n Number of quantiles (parametric)
#' @param B Sample size (parametric model)
#' @param P Quantiles of exposure in baseline (non-parametric model)
#' @param Q Quantiles of exposure in scenario (non-parametric model)
#' @param R Relative risk function in terms of total exposure (physical activity)
#' @param mean.Tc Mean value of baseline physical activity (not used when Tc.method = "Multinomial")
#' @param sd.Tc Standard deviation of baseline physical activity (not used when Tc.method = "Multinomial")
#'
#' @return The population attributable fraction
CRA.function <- function(meanlog.baseline = log(5),
                         sdlog.baseline = 1,
                         participation.baseline = 0.5,
                         meanlog.scenario = log(5),
                         sdlog.scenario = 1,
                         participation.scenario = 0.5,
                         Tc.method = "Multinomial", # Non-travel (Tc) distribution
                         author = "Arem",
                         income = NULL,
                         mean.Tc = 10,
                         sd.Tc = 1,
                         n = 1e4,
                         B = 1e4,
                         P,
                         Q,
                         R = ERF.acm.arem.fun) # Arem exposure-response function
{
  # Uniform distribution used for random sampling
  runif <- runif(n = B)
  
  # Generate baseline PA/LTPA (dependent on author, method)
  # Tc means the complement of Travel activity, i.e., non-travel activity
  if( Tc.method == "Non-random" ){
    PA.baseline <- mean.Tc
  }else if( Tc.method == "Multinomial" ){ # preferred method - 3/20/2019 HCF
    P <- getPActivity(author, income) # activity distribution dictated by author selection
    PA.baseline <- sample(P$bins, size = B, prob = P$probs, replace = TRUE)  # generate baseline values
  }else if( Tc.method == "Normal" ){
    PA.baseline <- rnorm(n = B, mean = mean.Tc, sd = sd.Tc)
  }else if( Tc.method == "Log-normal" ){
    # Simulated distribution for activity
    meanlog.Tc <- log(mean.Tc^2 / sqrt(sd.Tc^2 + mean.Tc^2))
    sdlog.Tc <- sqrt(log(1 + (sd.Tc / mean.Tc)^2))
    PA.baseline <- rlnorm(n = B, meanlog = meanlog.Tc, sdlog = sdlog.Tc)
  }else{ stop("Error with Tc.method argument: ", Tc.method) }
  
  # Initialize P distribution for relative risk calculation
  P <- quantile(PA.baseline, probs = (1:(n-1))/n)
  
  # Generate scenario PA/LTPA (dependent on author)
  TA.sample.baseline <- rlnorm(n = B, meanlog = meanlog.baseline, sdlog = sdlog.baseline)
  TA.sample.scenario <- rlnorm(n = B, meanlog = meanlog.scenario, sdlog = sdlog.scenario)
  # calculate change in participation (scenario vs baseline)
  participation.change <- participation.scenario - participation.baseline
  # if decrease in participation, decrease in activity
  if( participation.change < 0 ){ TA.sample.scenario <- -1*TA.sample.scenario }
  # generate scenario PA from baseline and simulated TA
  PA.scenario <- ifelse(PA.baseline + TA.sample.scenario > 0, PA.baseline + TA.sample.scenario, 0)
  # for proportion of population (if participation change, diff in participation baseline vs scenario) incorporate change in activity
  if( participation.change != 0 ){
    PA.scenario <- ifelse(runif > abs(participation.change), PA.baseline, PA.scenario)
  }else{
    PA.scenario <- ifelse(runif < participation.baseline, PA.baseline + TA.sample.scenario - TA.sample.baseline, PA.baseline)
    PA.scenario <- ifelse(PA.scenario <= 0, 0, PA.scenario)
  }
  # Initialize Q distribution for relative risk calculation
  Q <- quantile(PA.scenario, probs = (1:(n-1))/n)
  
  return((sum(R(Q)) - sum(R(P)))/sum(R(P)))
}



#' Generate log normal distribution of intensity values
#'
#' This function accepts 3 parameters, all numeric, for generation of a log normal distribution representing intensity of active travel.

#' @param n Number of participants
#' @param mean Mean intensity of observed travel activity 
#' @param sd Standard deviation of observed travel activity
#' 
#' @return Vector of values log-normally distributed
#' @export
HOT.rlnorm <- function(n, mean, sd){
  meanlog <- log(mean^2/sqrt(mean^2 + sd^2))
  sdlog <-  sqrt(log(1 + sd^2/mean^2))
  vec <- rlnorm(n = n, meanlog = meanlog, sdlog = sdlog)
  return(vec)
}
#' Generate travel activity distribution
#'
#' This function accepts 4 parameters, all numeric, for generation of a distribution representing travel activity. 
#'
#' @param participation Observed value for participation in active travel. Between 0 and 1. 
#' @param n Number of participants
#' @param intensity Mean intensity of observed travel activity 
#' @param sd Standard deviation of observed travel activity
#' 
#' @return Vector of values representing travel activity. 
#' @export
HOT.TA <- function(participation, intensity, sd, n){
  participation.vec <- as.numeric(runif(n = n) < participation)
  intensity.vec <- HOT.rlnorm(n = n, mean = intensity, sd = sd)
  TA.vec <- participation.vec*intensity.vec
  return(TA.vec)
}
#' Generate physical activity step-wise intervals
#'
#' This function accepts 3 parameters, all numeric, for generation of distribution along physical activity intervals for step-wise PA function.
#'
#' @param left Lower bound of interval
#' @param right Upper bound of interval
#' @param n Number of participants
#' 
#' @return Vector of n values within given interval. 
#' @export
HOT.interval <- function(left, right, n){
  if(left != right){ x <- runif(n = n, min = left, max = right)
  }else{ x <- rep(left, n) }
  return(x)
}
#' Generate leisure activity distribution
#'
#' This function accepts 1 numeric parameter for generation of leisure-time physical activity distribution.
#'
#' @param n Number of participants
#' 
#' @return Vector of values representing leisure time physical activity. 
#' @export
HOT.LA <- function(n){
  
  pa.df <- getPActivity(author = "Arem")
  intervals <- cbind(matrix(c(0,0,0,7.5,7.5,15,15,22.5,22.5,40,40,75,75,100), byrow = TRUE, ncol = 2), as.integer(pa.df$probs*n))
  
  LA.vec <- c()
  for(i in 1:nrow(intervals)){
    LA.vec <- c(LA.vec, HOT.interval(left = intervals[i,1], right  = intervals[i,2], n = intervals[i,3]))
  }
  
  return(LA.vec)
}
#' Generate physical activity distribution
#'
#' This function accepts 4 parameters, all numeric, for generation of physical activity distribution. It combines leisure-time physical activity and travel activity.
#'
#' @param participation Numeric value for the participation estimate
#' @param intensity Numeric value for the intensity estimate
#' @param sd Numeric value for the estimate of standard deviation of travel activity
#' @param n Number of participants
#' 
#' @return Vector of values representing physical activity distribution. 
#' @export
HOT.PA <- function(participation = 0.6, intensity = 2, sd  = 1, n = 1e6){
  
  LA <- HOT.LA(n = n)
  TA <- HOT.TA(participation = participation, intensity = intensity, sd = sd, n = length(LA))
  PA <- LA + TA
  
  return(PA)
}
#' Calculate Comparative Risk Assessment for Travel Survey Object
#'
#' This function accepts 3 parameters. It accepts a TravelSurvey class object for performing CRA across locations within that object. A user may provide 'n' and 'nq' for the generated travel and leisure time physical activity distributions, respectively. 
#'
#' @param parameters.df Data frame whose rows represent subgroups under analysis, including three columns labeled: "participation", "intensity", "sigma"
#' @param n Sample size for generated travel activity distribution
#' @param nq Sample size for generated leisure time physical activity distribution
#' 
#' @return Data frame of HOT parameters with location, participation, intensity, sd, and rho (CRA result). 
#' @export
HOT.CRA <- function(parameters.df, n = 1e6, nq = 1e4){
  if( length(parameters.df[,1] == 1) ){ stop("The parameters.df input only has one row.") }
  
  LA.baseline  <- HOT.LA(n = n)
  n <- length(LA.baseline)
  TA.baseline <- HOT.TA(participation = HOT.parameters.df[1,"participation"], intensity = HOT.parameters.df[1,"intensity"], sd = HOT.parameters.df[1,"sigma"], n = n)
  q.baseline <- quantile(LA.baseline, probs = (1:nq)/nq)
  
  rho.vec <- c()
  for(i in 2:nrow(parameters.df)){
    TA.scenario <- HOT.TA(participation = HOT.parameters.df[i,"participation"], intensity = HOT.parameters.df[i,"intensity"], sd = HOT.parameters.df[i,"sigma"], n = n)
    deltaTA <- TA.scenario - TA.baseline
    LA.scenario <- ifelse(LA.baseline + deltaTA < 0, 0, LA.baseline + deltaTA)
    q.scenario <- quantile(LA.scenario, probs = (1:nq)/nq)
    
    rho <- sum(ERF.acm.arem.fun(q.scenario))/sum(ERF.acm.arem.fun(q.baseline)) - 1
    rho.vec <- c(rho.vec, rho)
  }
  parameters.df <- parameters.df %>% mutate(rho = c(NA, rho.vec))
  return(parameters.df)
}


