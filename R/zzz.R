# Define HOT package signature
THISPKG <- "HOT"
.onAttach <- function(libname, pkgname) {
    version <- packageDescription("HOT", fields="Version")
    #date <- packageDescription("HOT", fields="Date")
    packageStartupMessage(paste("Welcome to HOT version ", version, ".\n",
                                "~~~~~~~~~~~~~~~~~", "\n",
                                ">()_  >()_  >()_ ", "\n",
                                " (__)  (__)  (__)", "\n",
                                "~~~~~~~~~~~~~~~~~", sep = "" ) )
    }
